

class LoginPage:
    def __init__(self, driver):
        self.driver = driver

    def openStagingLoginPage(self):
        self.driver.get('https://staging2.emplocity.com/apps/sourceapp/login')

    def enterLogin(self):
        self.driver.find_element_by_name("email").send_keys("waclaw.zurko.user@emplocity.pl")

    def enterPass(self):
        self.driver.find_element_by_name("password").send_keys("Tester9191!")

    def clickLogin(self):
        self.driver.find_element_by_xpath('//*[@id="root"]/div/div[2]/div[2]/button').click()
