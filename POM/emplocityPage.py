from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException


class EmplocityPage:
    def __init__(self, driver):
        self.driver = driver

    def openWebPage(self):
        self.driver.get('https://emplocity.com/pl/')
        timeout = 5
        try:
            element_present = EC.presence_of_element_located((By.ID, 'demo-btn'))
            WebDriverWait(self.driver, timeout).until(element_present)
        except TimeoutException:
            print("Timed out waiting for page to load")

    def closeCookies(self):
        adres = 'BtnCookies'
        WebDriverWait(self.driver, 10).until(EC.element_to_be_clickable((By.ID, adres)))
        self.driver.find_element_by_id(adres).click()

    def clickArrangeDemo(self):
        self.driver.execute_script("document.getElementById('demo-btn').click();")

    def asserDemoPopup(self):
        adres = '/html/body/form/div'
        self.driver.switch_to.frame(self.driver.find_element_by_xpath('//*[@id="smIframe-mseonnb01xngz1kd"]'))
        WebDriverWait(self.driver, 10).until(EC.visibility_of_all_elements_located((By.XPATH, adres)))
        self.driver.find_element_by_xpath(adres).is_displayed()
