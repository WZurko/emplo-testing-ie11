from utilities.openBrowser import LoginForm
from POM.emplocityPage import EmplocityPage
import unittest


class ArrangeDemo(unittest.TestCase):
    def test_demo(self):
        inic = LoginForm()

        driver = inic.setUp()

        emplo_page = EmplocityPage(driver)

        emplo_page.openWebPage()
        emplo_page.closeCookies()
        emplo_page.clickArrangeDemo()
        emplo_page.asserDemoPopup()


        inic.tearDown()


if __name__ == '__main__':
    # test_login()
    unittest.main()
