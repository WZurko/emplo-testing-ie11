import unittest
from selenium import webdriver
import requests


class LoginForm(unittest.TestCase):
    def setUp(self):

        self.username = "arkadiusz.talun@emplocity.pl"
        self.authkey = "uf8066a900d08a6a"

        self.api_session = requests.Session()
        self.api_session.auth = (self.username, self.authkey)

        self.test_result = None

        caps = {}

        caps['build'] = '1.0'
        caps['browserName'] = 'Internet Explorer'
        caps['version'] = '11'
        caps['platform'] = 'Windows 10'
        caps['screenResolution'] = '1366x768'
        caps['record_video'] = 'true'
        caps['record_network'] = 'false'

        self.driver = webdriver.Remote(
            desired_capabilities=caps,
            command_executor="http://%s:%s@hub.crossbrowsertesting.com:80/wd/hub" % (self.username, self.authkey)
        )

        self.driver.implicitly_wait(20)

        return self.driver

    def tearDown(self):
        print("Done with session %s" % self.driver.session_id)
        self.driver.quit()
