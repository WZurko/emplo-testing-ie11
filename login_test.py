from POM.loginPage import LoginPage
from utilities.openBrowser import LoginForm
from POM.dashboardPage import DashboardPage
import unittest


class TestLogin(unittest.TestCase):
    def test_login(self):
        inic = LoginForm()

        driver = inic.setUp()

        login = LoginPage(driver)
        login.openStagingLoginPage()
        login.enterLogin()
        login.enterPass()
        login.clickLogin()
        dashboard = DashboardPage(driver)
        dashboard.assertLogedIn()

        inic.tearDown()


if __name__ == '__main__':
    # test_login()
    unittest.main()
